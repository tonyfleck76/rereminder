# ReReminder - A Better Way to Plan #

## Illinois Institute of Technology - CS487 ##

### Students ###

Anthony Fleck, Chunhao Li, Francesco Fanizza, and Thibault Timmerman

## Building The Backend ##

Below are the instructions for building and running the Rereminder backend.

###I.	Installing Java and Maven###

Both Java and Maven can be installed using apt-get.

`apt-get install openjdk-8-jdk`

`apt-get install maven`

These will both be needed to build the Rereminder server.

###II.	Clone the repository from Bitbucket###

The repository can be found at https://tonyfleck76@bitbucket.org/tonyfleck76/rereminder.git

It can be cloned with the following command:
`git clone https://tonyfleck76@bitbucket.org/tonyfleck76/rereminder.git`

###III.	Build the Backend###

Before building the project, ensure that you are in the same directory as the POM.xml. This is located under `./rereminder/Backend/ReReminder`

The code can be built using maven. The following command can be used to generate the code coverage for the backend.

`mvn test`

This command builds the code and runs all unit tests. The coverage report is located in the ./target/site/jacoco-ut directory. The file “index.html” contains the main page for the coverage report, and the other folders contain individual reports for each java package and class. If you wish to view the coverage on a different machine, make sure you copy the whole directory, and not just the index.html file.

`mvn spring-boot:run`

This command compiles the code and starts the server running.